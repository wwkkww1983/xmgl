# Saas+Paas平台部署全生命周期AIOT智能物联网工程项目管理

#### 介绍
项造营是第一款国产的半开源B/S组件化Saas+Paas+AIOT智能物联网工程项目管理软件，涉及的业务模块涵盖项目投标、项目合同、成本预算、物资供应链、劳务分包、机械租赁、施工进度、质量及安全、资金计划、风险管理、项目运维、财务核算、变更管理、项目人力资源管理、预警提醒、移动APP、组织结构管理，角色管理、用户组管理、岗位管理、权限管理，Saas多租户管理，表单配置、门户配置、工作流引擎、第三方集成引擎，组件化引擎(在线发布组件化代码，后台动态编译，无需安装开发工具)，智慧工地涵盖人员管理(劳务实名制、VR安全教育、智能安全帽),绿色施工(环境监测、喷淋系统、车辆管理),物料管理(智能地磅),生产管理(BIM管理、进度管理、任务管理、可视对讲),现场监控(视频监控、航拍全景),质量安全(用电管理、智能烟感、深基坑监测、高支模监测、质量安全巡检),设备管理(塔吊安全辅助、升降机安全辅助、卸料平台安全辅助)，通过智能化智慧工地监管平台、不但对企业管控、项目人员管理、质量安全管控等提供安全有效的技术做支撑，并提供后台大数据分析，帮助集团或项目掌控核心大数据。帮助其提升各项管理指标的同时，并有效积累优质劳务班组数据，有效解决未来工人荒核心问题。
基于项造营平台搭建的工程项目管理产品适用行业：基建工程、PPP项目、EPC、园林绿化、土建施工、环境工程、电力工程、智能工程、铁道交通、城市轨道、公路桥梁、建筑装饰、监控通讯、系统集成、市政工程、光伏照明、新能源等。
可以使用项造营Paas平台根据企业需求搭建不用行业的应用，同时支持部署saas到自已的服务器（采用一个租户一个数据库架构，方便后续不同租户的个性化修改）
项造营官方网站： http://www.blacknest.cn
请通过官方渠道联系技术支持人员获取。

#### 解决工程项目哪些问题？
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133652_bf0072e2_1481027.png "11.png")

#### 产品概述
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133718_e3979bc4_1481027.png "13.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133728_6ca26d21_1481027.png "14.png")
#### 驾驶舱
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133341_88a867bd_1481027.png "19.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133359_bc513db7_1481027.png "4.png")

#### 自动化预警平台
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133423_7abc4cf6_1481027.png "21.png")

#### 自定义预警配置
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133437_d33eeb20_1481027.png "22.png")

#### 360°项目全景视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133544_3e7c052e_1481027.png "5.png")

#### 投标管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/133921_168d57de_1481027.png "p17.png")
#### 投标视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/084856_1b4ea04e_1481027.png "投标管理.png")
#### 合同管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142159_e5436c9c_1481027.png "14.png")
#### 合同视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/084953_1924c2d4_1481027.png "合同视图.png")
#### 成本预算
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142321_113b1a07_1481027.png "15.png")
#### 成本预算视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/085108_759ac82a_1481027.png "成本预算.png")
#### 物资采购及供应链管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142351_293df282_1481027.png "16.png")
#### 物资采购视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/085234_3cf57a4e_1481027.png "材料采购.png")
#### 劳务分包管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142413_fd3d32e3_1481027.png "17.png")
#### 劳务分包视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/085330_5519e45d_1481027.png "劳务分包视图.png")
#### 租赁管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142440_60d47785_1481027.png "19.png")
#### 租赁视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/085421_02fc7aa2_1481027.png "租赁视图.png")
#### 成本费用、成本费用流程、项目费用报销、非项目费用报销
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142513_d86bd9ca_1481027.png "20.png")
#### 成本费用视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/085522_8ba233fd_1481027.png "成本费用.png")
#### 施工进度管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142531_c05d80de_1481027.png "21.png")
#### 施工视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/085628_e7b73b07_1481027.png "施工视图.png")
#### 施工进度
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/085748_46d84b96_1481027.png "施工进度.png")
#### 施工计划甘特图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/085844_823e644e_1481027.png "施工甘特图.png")
#### 运维管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142559_7a44f24b_1481027.png "22.png")
#### 运维管理视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/090421_06a4ab40_1481027.png "竣工运维.png")
#### 质量与安全控制
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142631_66f9a99f_1481027.png "23.png")
#### 质量与安全控制视图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/090454_58ec305e_1481027.png "安全管理.png")
#### 财务核算管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142700_c3fd55e7_1481027.png "24.png")
#### 财务管理
#### 1、开票收款
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/084434_a486d4c8_1481027.png "财务开票收款.png")
#### 2、收票付款
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/084502_5f71991b_1481027.png "财务收票付款.png")
#### 3、收支合同
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/084527_0dabaa4f_1481027.png "财务收支合同.png")
#### 4、保证金
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/084550_96c85258_1481027.png "财务保证金.png")
#### 5、项目库存
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/084616_b7e01c62_1481027.png "财务项目库存.png")
#### 6、资金计划
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/084642_150ea9ec_1481027.png "资金计划.png")
#### 文档管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/142841_5369c217_1481027.png "p42.png")
#### 文档管理视图(分不同项目阶段，自动文档归集)
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/090542_edb38977_1481027.png "项目文档.png")
#### Saas平台(数据隔离、多租户、可配置、可扩展、热部署)
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/143014_6ca376b7_1481027.png "saas.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/143035_f13b4a64_1481027.png "p50.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/143045_e902ff1e_1481027.png "p51.png")
#### 多种界面风格选择
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/101446_e560f209_1481027.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/101549_26417467_1481027.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/101637_7f3421fd_1481027.png "屏幕截图.png")
#### 智慧工地
![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/235424_c450d6ee_1481027.png "屏幕截图.png")
#### 智能化监控平台
![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/235555_1d4fd6bc_1481027.png "屏幕截图.png")
#### 多种终端支持，方便管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/235653_970c281a_1481027.png "屏幕截图.png")
#### 自定义项目与施工现场集成
![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/235738_43a799ab_1481027.png "屏幕截图.png")
#### 员工考勤数据通过人脸识别实时更新
![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/235816_8f0ba570_1481027.png "屏幕截图.png")
#### 直通施工现场
![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/235909_9f4ec7dd_1481027.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/235927_177add8e_1481027.png "屏幕截图.png")
#### 灵活配置数据大屏
![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/235957_8cee5318_1481027.png "屏幕截图.png")
#### 实现安全、质量、进度、成本、环境保护管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0504/000103_bf1e9f8b_1481027.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0504/000120_cca9022a_1481027.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0504/000146_4c0dd6f1_1481027.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0504/000204_f6cba185_1481027.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0504/000220_54756b96_1481027.png "屏幕截图.png")
#### 智能安全帽
![输入图片说明](https://images.gitee.com/uploads/images/2021/0504/000250_6bf5127f_1481027.png "屏幕截图.png")
#### 塔吊安全监测——吊钩可视化、多机防碰撞
![输入图片说明](https://images.gitee.com/uploads/images/2021/0505/112832_121fbb2f_1481027.png "屏幕截图.png")
#### 施工升降机安全监测
![输入图片说明](https://images.gitee.com/uploads/images/2021/0505/112907_5b5e1a8e_1481027.png "屏幕截图.png")
#### 卸料平台安全监测
![输入图片说明](https://images.gitee.com/uploads/images/2021/0505/112944_f8b05d65_1481027.png "屏幕截图.png")
#### 用电成本与安全监测
![输入图片说明](https://images.gitee.com/uploads/images/2021/0505/113014_f4557a64_1481027.png "屏幕截图.png")
#### 深基坑安全监测
![输入图片说明](https://images.gitee.com/uploads/images/2021/0505/113048_dacc6a05_1481027.png "屏幕截图.png")
了解更多智慧工地，请资询官方售前
#### 官方微信交流群
备注：开源中国
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/131424_2b82f400_1481027.png "code.png")
